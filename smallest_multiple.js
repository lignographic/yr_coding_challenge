//////////////////////
/////////////
// Test A :
// 2520 is the smallest number that can be divided by each of
// the numbers from 1 to 10 without any remainder.

// Find the smallest positive number that is evenly divisible by
// all of the numbers from 1 to 20.


 function smallMultiple(max){
 	var result = 0;
 	var count = 0;
 	var found = false;
 	var i = 0;

 	while(found != true){
 		i += 1;
 		for(var j = 1; j <= max; j++){
 			if(i%j == 0){
 				count += 1;
 				if(count == max){
 					found = true;
 					result = i;
 					return 'the smallest number that we are searching for is : ' + result;
 				}
 			}
 			else {
 				found = false;
 				count = 0;
 			}
 		}
 	}
 }
 console.log(smallMultiple(20));