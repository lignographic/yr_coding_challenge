/////////////
// Test B :
// By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13,
// we can see that the 6th prime number is 13.

// Find the 10 001st and 100 001st prime number.

/////
// PRIME NUMBER - DEFINITION :
// a prime is a number evenly divisible by 1 or itself
/////
// we start i at 2 because division by 0 is impossible
// and 1 is neither prime or composite
// any whole number greater than 1 is either prime or composite


 function prime(n){
 	if(n < 2){
 		return false;
 	}
 	for(var i = 2; i < n; i++){
 		if(n % i == 0){
 			return false;
 		}
 	}
 	return true;
 }

 function searchPrime(num){
 	var i = 0;
 	var primes = [];
 	var count = 0;
 	while(count != num){
 		i += 1;
 		prime(i);
 		if(prime(i)){
 			count+= 1;
 			primes.push(i);
 		}

 	}
 	return 'the ' + num + ' prime is : ' + primes[primes.length-1];
 }

 console.log(searchPrime(10001));