# Coding challenge and JS questions

This repository contains the answers for the test coding (A & B) and the test JS (questionnaire).

## Test coding

The smallest_multiple.js file contains the answer for the test coding A - smallest multiple.
The prime_numbers.js file contains the answer for the test coding B - Prime numbers.

## Test JS

The js_questions.js contains answers for the questionnaire test JS