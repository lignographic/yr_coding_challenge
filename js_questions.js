/////////////
/////////////
// JS QUESTIONS

////////
// .1

// for(var i = 0; i < 5; i++){
// 	console.log(i);
// }

// it will log equal 0 to 4
// 0
// 1
// 2
// 3
// 4


////////
// .2

// for(var i = 0; i < 5; i++){
// 	setTimeout(function(){
// 		console.log(i);
// 	}, 1000);
// }

// after one seconde, it will log five times i, which is equal to 5
// 5
// 5
// 5
// 5
// 5

// because the console.log is in the callback of the setTimeout function,
// the loop is already finished by the time the asynchronous function its rnning.
// Value i will be equal to 5. The logs will be return, according the slack with
// i equal to 5


////////
// .3

// for(let i = 0; i < 5; i++){
// 	setTimeout(function(){
// 		console.log(i);
// 	}, 1000);
// }

// node --use_strict test_js.js
// 0
// 1
// 2
// 3
// 4

// let(ECMAScript6) provides a new scope for i, ('block scope' does not exist
// in JavaScript)
// Now the value of i is increasing by 1 each time according the for loop


////////
// .4

// for(var i = 0; i < 5; i++){
// 	var j = i;
// 	setTimeout(function(){
// 		console.log(j);
// 	}, 1000);
// }

// 5
// 5
// 5
// 5
// 5

// same problem than we had in exemple 2, J refered to i that is increasing
// according the for loop, which is already finish by the time the setTimeout function
// is running


////////
// .5

// for(var i = 0; i < 5; i++){
// 	function indexPrinter(){
// 		var j = i;
// 		return function(){
// 			setTimeout(function(){
// 				console.log(j);
// 			}, 1000);
// 		}
// 	}
// 	var myIndex = indexPrinter;
// 	myIndex();
// }

// 0
// 1
// 2
// 3
// 4

// the function indexPrinter creates a new context, a new scope for j that is equal to i.
// each time in the for loop, myIndex (which's refered to indexPrinter function) is fired,
// an anonymous function that's contain a asynchronous function.
// then i is inscreasing by and j also (because its refer to i). Repeat since i < 5.
// after the for loop finished, one second later, the slack is executed and returns j, five
// time, according the number of loops.

////////
// .5-b

// the inner function uses i:function(){ console.log(i);}, 1000}
// did not understand the question here (?)





